const defaulState = {
    open: false,
    content: {}
}

const modal = (state = defaulState, action) => {
    switch (action.type) {
        case 'OPEN_MODAL':
           state = { ...state, open: true, content: action.payload }
           break;
        
        case 'CLOSE_MODAL':
            state = { ...state, open: false, content: {} }
           break;
    }

    return state;
}

export default modal