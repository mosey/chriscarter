const defaulState = {
    playing: false,
    id: null
}

const audio = (state = defaulState, action) => {
    switch (action.type) {
        case 'PLAY_AUDIO':
            state = { ...state, playing: true, id: action.payload }
            break;

        case 'STOP_AUDIO':
            state = { ...state, playing: false, id: null }
            break;
    }

    return state;
}

export default audio