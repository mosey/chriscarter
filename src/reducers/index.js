import { combineReducers } from 'redux'
import modal from './modal'
import audio from './audio'

export default combineReducers({
    modal,
    audio
})