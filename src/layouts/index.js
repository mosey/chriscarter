import React from 'react'
import Helmet from 'react-helmet'
import {Provider} from 'react-redux'
import store from '../store'
import favicon from '../images/favicon.ico'

import '../sass/main.scss'

// To do: 
// Set up modal functionality:
// -- Click to open / close 
// -- Manage state
// Set up audio playing
// -- Dismount after finish playing / loop audio / stop audio
// Set up redux?

class TemplateWrapper extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Provider store={store}>
        <div>
          <Helmet
            title="Chris Carter Periodic Table"
            meta={[
              { name: 'description', content: 'Chris Carter New Album' },
              { name: 'keywords', content: 'Chirs Carter, Musician' },
            ]}
          >
              <link rel="icon" href={favicon} type="image/x-icon" />
              <script type="text/javascript">
              {/* <![CDATA[ */}
              var google_conversion_id = 1031663084;
              var google_conversion_label = "QjykCNuIm38Q7Nv36wM";
              var google_custom_params = window.google_tag_params;
              var google_remarketing_only = true;
              {/* ]]> */}
            </script>

            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js" />
            <noscript>
              {`<div style={{display: 'inline'}}>
                <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1031663084/?label=QjykCNuIm38Q7Nv36wM&amp;guid=ON&amp;script=0"/>
              </div>`}
            </noscript>
          </Helmet>
            {
              this.props.children()
            }
        </div>
      </Provider>
    )
  }

}

export default TemplateWrapper
