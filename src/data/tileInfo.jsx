import React from 'react'
import YouToob from '../components/YouToob'

export const tileInfo = {
    moonTwo: {
        ytId: 'tflile4Wh7o',
        text: <div>
            <p>
                Moon Two - the third track to be revealed from CCCL, available on a new moon no less, the video was created by Chris Carter.
            </p>
            <p>
                Chris Carter’s Chemistry Lessons (CCCL) Volume One was created during the period 2010 to 2017. The majority of the album was recorded using various modular synthesisers but the working process also included drum machines, digital synthesis and digital signal processing (DSP), voice resynthesis, custom electronics and field recordings. The album was edited and mastered using a computer based digital audio workstation (DAW). The resulting tracks are a hybridisation of unique tonalities, resonances, timbres, sounds and melodies.
            </p>
            <p><a href="http://smarturl.it/CCCLV1" target="_blank">&lt;Listen / Buy&gt;</a></p>
        </div>
    },
    cernubicua: {
        ytId: 'HC46nk5noC0',
        text: <div>
            <p>Cernubicua - the second track to be revealed from CCCL, the video was created by Chris Carter.</p>
            <p>Chris Carter’s Chemistry Lessons (CCCL) Volume One was created during the period 2010 to 2017. The majority of the album was recorded using various modular synthesisers but the working process also included drum machines, digital synthesis and digital signal processing (DSP), voice resynthesis, custom electronics and field recordings. The album was edited and mastered using a computer based digital audio workstation (DAW). The resulting tracks are a hybridisation of unique tonalities, resonances, timbres, sounds and melodies.</p>
            <p><a href="http://smarturl.it/CCCLV1" target="_blank">&lt;Listen / Buy&gt;</a></p>
        </div>
    },
    blisster: {
        ytId: 'o93kEo1jBbo',
        text: <div>
            <p>Blissters - the first track to be revealed from CCCL, the video was created by Chris Carter.</p>
            <p>Chris Carter’s Chemistry Lessons (CCCL) Volume One was created during the period 2010 to 2017. The majority of the album was recorded using various modular synthesisers but the working process also included drum machines, digital synthesis and digital signal processing (DSP), voice resynthesis, custom electronics and field recordings. The album was edited and mastered using a computer based digital audio workstation (DAW). The resulting tracks are a hybridisation of unique tonalities, resonances, timbres, sounds and melodies.</p>
            <p><a href="http://smarturl.it/CCCLV1" target="_blank">&lt;Listen / Buy&gt;</a></p>
        </div>
    },
    chemistryLessons: {
        ytId: '86bGd7uR-9Y',
        text: <div>
            <p>
                Here is the first in a series of videos where Chris discusses areas of interest. In Chemistry Lessons Video 1 Chris discusses his thought process regarding the artwork for his new album Chris Carter’s Chemistry Lessons Volume One. 
            </p>
            <p><a href="http://smarturl.it/CCCLV1" target="_blank">&lt;Listen / Buy&gt;</a></p>
            <br/>
            <div className="c-modal__video-wrapper">
                <YouToob videoId='IaWWezA5F1U' />
            </div>
            <p>
                Here is the second in a series of videos where Chris discusses areas of interest. In Chemistry Lessons Video 2 Chris goes through the wide range of effects pedals used for his new album Chris Carter’s Chemistry Lessons Volume One.
            </p>
            <p><a href="http://smarturl.it/CCCLV1" target="_blank">&lt;Listen / Buy&gt;</a></p>
            <br />
            <div className="c-modal__video-wrapper">
                <YouToob videoId='X8RCpbElxmo' />
            </div>
            <p>
                Here is the third in a series of videos where Chris discusses areas of interest. In Chemistry Lessons Video 3 Chris goes through the recording and mixing process for his new album Chris Carter’s Chemistry Lessons Volume One.

            </p>
            <p><a href="http://smarturl.it/CCCLV1" target="_blank">&lt;Listen / Buy&gt;</a></p>
            <br/>
            <div className="c-modal__video-wrapper">
                <YouToob videoId='0BvOS0ezcYY' />
            </div>
            <p>
                Here is the fourth in a series of videos where Chris discusses areas of interest. In Chemistry Lessons Video 4 Chris gives us a tour of his studio hardware and the vast array of equipment used on his new album Chris Carter’s Chemistry Lessons Volume One.
            </p>
            <p><a href="http://smarturl.it/CCCLV1" target="_blank">&lt;Listen / Buy&gt;</a></p>
        </div>
    },
    variousInfluences: {
        text: <div>
            <p>
                <a href="https://www.bbc.co.uk/programmes/b09vp6k0" target="_blank">
                    Interview - Nemone's Electric Ladyland - 6 Music
                </a>
            </p>
            <p>
                <a href="https://soundcloud.com/synthheroshow/chris-carter-synth-hero-mix-1" target="_blank">
                    Mix - Synth Hero Show - NTS Radio
                </a>
            </p>
            <p>
                <a href="https://www.mixcloud.com/NTSRadio/bleep-w-chris-carter-28th-february-2018/" target="_blank">
                    Mix - Bleep - NTS Radio
                </a>
            </p>
        </div>
    },

    variousPublicity: {
        text: <div>
            <p>
                <a href="https://bleep.com/Albums-of-the-year-2018-Chris-Carter" target="_blank">
                    <b>#5 in Bleep's Album of the Year list 2018</b>
                </a>
                <br/><br/>
                Chris Carter's Chemistry Lessons Volume 1 is an album of the year in Bleep, The Quietus, Mojo, Cold War Night Life, Electronic Sound, Rough Trade, Drift, Louder Than War and Uncut
            </p>
            <p>
                <a href="http://thequietus.com/articles/24232-chris-carter-marta-salogni-interview " target="_blank">
                    <b>The Quietus:</b> &nbsp;Long Lunch: Chris Carter &amp; Marta Salogni Talk Happy Accidents 
                </a>
            </p>
            <p>
                <a href="https://www.resolutionmag.com/uncategorised/chris-carter-channels-vintage-electronica-and-folk-for-exquisite-solo-return/" target="_blank">
                    <b>Resolution Magazine:</b> &nbsp;Chris Carter Channels Vintage Electronica And Folk For Exquisite Solo Return
                </a>
            </p>
            <p>
                <a href="https://www.readersdigest.co.uk/culture/music/chris-carter-records-that-changed-my-life" target="_blank">
                    <b>Readers Digest:</b> &nbsp;Chris Carter: Records That Changed My Life
                </a>
            </p>
            <p>
                <a href="https://www.interviewmagazine.com/music/throbbing-gristle-co-founder-chris-carter-still-pushing-limits" target="_blank">
                    <b>Interview Magazine:</b> &nbsp;Throbbing Gristle Co-Founder Chris Carter Is Still Pushing The Limits
                </a>
            </p>
        </div >   
    },

    liveEvents: {
        text: <div>
            <p>
                <a href="https://vauxhalltavern.ticketsolve.com/shows/1173587897" target="_blank">London, The Vauxhall Tavern, 23 May</a>
            </p>
            <p>
                <a href="https://www.seachangefestival.co.uk/2018-artists/" target="_blank">Totnes, Sea Change Festival, 24/25 August</a>
            </p>
        </div>
    },

    tangerines: {
        text: <div>
            <p>
                Coming soon…
            </p>
        </div>
    }


    

}
