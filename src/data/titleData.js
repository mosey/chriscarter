export const tileData = [
    {
        title: 'Duration Increasing >',
        color: 'green',
        text: 'left'
    },

    {
        blank: true
    },

    {
        blank: true
    },

    {
        chemName: 'Po',
        chemNum: '19',
        title: 'Post Industrial',
        time: '3.17',
        color: 'blue',
        type: 'audio',
        audioId: '19_PostIndustrial',
        infoId: null
    },

    {
        chemName: 'Ce',
        chemNum: '04',
        title: 'Cernubicua',
        time: '3.30',
        color: 'blue',
        type: 'audio',
        audioId: '04_Cernubicua',
        infoId: null
    },

    {
        chemName: 'Co',
        chemNum: '10',
        title: 'Corvus',
        time: '3.51',
        color: 'blue',
        type: 'audio',
        audioId: '10_Corvus',
        infoId: null
    },

    {
        chemName: 'Mo',
        chemNum: '06',
        title: 'Modularity',
        time: '4.47',
        color: 'blue',
        type: 'audio',
        audioId: '06_Modularity',
        infoId: null
    },
    
    {
        blank: true
    },
    {
        blank: true
    },

    {
        chemName: 'No',
        chemNum: '18',
        title: 'Noise Floor',
        time: '2.43',
        color: 'blue',
        type: 'audio',
        audioId: '18_NoiseFloor',
        infoId: null
    },

    {
        chemName: 'Bl',
        chemNum: '1',
        title: 'Blissters',
        time: '2.48',
        color: 'blue',
        type: 'audio',
        audioId: '01_Blissters',
        infoId: null
    },

    {
        chemName: 'Gh',
        chemNum: '17',
        title: 'Ghosting',
        time: '2.52',
        color: 'blue',
        type: 'audio',
        audioId: '17_Ghosting',
        infoId: null
    },

    {
        chemName: 'Ar',
        chemNum: '23',
        title: 'Ars Vetus',
        time: '2.57',
        color: 'blue',
        type: 'audio',
        audioId: '23_ArsVetus',
        infoId: null
    },

    {
        chemName: 'La',
        chemNum: '14',
        title: 'Lab Test',
        time: '3.13',
        color: 'blue',
        type: 'audio',
        audioId: '14_LabTest',
        infoId: null
    },

    {
        chemName: 'Du',
        chemNum: '12',
        title: 'Dust & Spiders',
        time: '1.40',
        color: 'blue',
        type: 'audio',
        audioId: '12_DustSpiders',
        infoId: null
    },

    {
        chemName: 'Gr',
        chemNum: '13',
        title: 'Gradients',
        time: '1.45',
        color: 'blue',
        type: 'audio',
        audioId: '13_Gradients',
        infoId: null
    },

    {
        chemName: 'In',
        chemNum: '25',
        title: 'Inkstain',
        time: '1.58',
        color: 'blue',
        type: 'audio',
        audioId: '25_Inkstain',
        infoId: null
    },

    {
        chemName: 'Ro',
        chemNum: '21',
        title: 'Roane',
        time: '2.00',
        color: 'blue',
        type: 'audio',
        audioId: '21_Roane',
        infoId: null
    },

    {
        chemName: 'Ti',
        chemNum: '22',
        title: 'Time Curious',
        time: 'Glows 2.10',
        color: 'blue',
        type: 'audio',
        audioId: '22_TimeCuriousGlows',
        infoId: null
    },

    {
        chemName: 'Re',
        chemNum: '20',
        title: 'Rehndim',
        time: '2.18',
        color: 'blue',
        type: 'audio',
        audioId: '20_Rehndim',
        infoId: null
    },

    {
        chemName: 'Pi',
        chemNum: '05',
        title: 'Pillars of Wah',
        time: '2.19',
        color: 'blue',
        type: 'audio',
        audioId: '05_PillarsOfWah',
        infoId: null
    },

    {
        chemName: 'Du',
        chemNum: '09',
        title: 'Durlin',
        time: '2.56',
        color: 'mustard',
        type: 'audio',
        audioId: '09_Durlin',
        infoId: null
    },

    {
        chemName: 'Sh',
        chemNum: '15',
        title: 'Shildreke',
        time: '3.04',
        color: 'mustard',
        type: 'audio',
        audioId: '15_Shildreke',
        infoId: null
    },

    {
        blank: true
    },

    {
        chemName: 'Ta',
        chemNum: 'v',
        title: 'Tangerines',
        time: '2.04',
        color: 'green',
        type: 'modal',
        infoId: 'tangerines'
    },

    {
        chemName: 'Mo',
        chemNum: 'v',
        title: 'Moon Two',
        time: '2.38',
        color: 'green',
        type: 'modal',
        infoId: 'moonTwo'
    },

    {
        chemName: 'Bl',
        chemNum: 'v',
        title: 'Blissters',
        time: '2.50',
        color: 'green',
        type: 'modal',
        infoId: 'blisster'
    },

    {
        chemName: 'Ce',
        chemNum: 'v',
        title: 'Cernubicua',
        time: '3.31',
        color: 'green',
        type: 'modal',
        infoId: 'cernubicua'
    },

    {
        chemName: 'Ni',
        chemNum: '03',
        title: 'Nineteen 7',
        time: '2.05',
        color: 'mustard',
        type: 'modal',
        type: 'audio',
        audioId: '03_Nineteen',
        infoId: null
    },

    {
        chemName: 'To',
        chemNum: '11',
        title: 'Tones Map',
        time: '2.14',
        color: 'mustard',
        type: 'audio',
        audioId: '11_TonesMap',
        infoId: null
    },

    {
        chemName: 'Mo',
        chemNum: '08',
        title: 'Moon Two',
        time: '2.35',
        color: 'mustard',
        type: 'audio',
        audioId: '08_MoonTwo',
        infoId: null
    },

    {
        blank: true
    },

    {
        chemName: 'Cl',
        chemNum: 'v',
        title: 'Various Chemistry Lessons',
        time: '',
        color: 'pink',
        type: 'modal',
        infoId: 'chemistryLessons'
    },

    {
        chemName: 'Au',
        chemNum: 'a',
        title: 'Various Influences',
        time: '',
        color: 'pink',
        type: 'modal',
        infoId: 'variousInfluences'
    },

    {
        chemName: 'Pr',
        chemNum: 'p',
        title: 'Various Publicity',
        time: '',
        color: 'pink',
        type: 'modal',
        infoId: 'variousPublicity'
    },

    {
        chemName: 'Ta',
        chemNum: '02',
        title: 'Tangerines',
        time: '2.00',
        color: 'mustard',
        type: 'audio',
        audioId: '02_Tangerines',
        infoId: null
    },

    {
        chemName: 'Uy',
        chemNum: '16',
        title: 'Uysring',
        time: '2.01',
        color: 'mustard',
        type: 'audio',
        audioId: '16_Uysring',
        infoId: null
    },

    {
        chemName: 'Ho',
        chemNum: '24',
        title: 'Hobbs End',
        time: '2.05',
        color: 'mustard',
        type: 'audio',
        audioId: '24_HobbsEnd',
        infoId: null
    },
    
    {
        chemName: 'Fi',
        chemNum: '07',
        title: 'Field Depth',
        time: '2.40',
        color: 'mustard',
        type: 'audio',
        audioId: '07_FieldDepth',
        infoId: null
    },

    {
        blank: true
    },

    {
        blank: true
    },

    {
        title: 'Duration Decreasing <',
        text: 'left',
        color: 'darkNavy'
    },

    {
        chemName: 'Bu',
        chemNum: 'y',
        title: 'Shop',
        time: 'CD / Vinyl',
        color: 'lightGrey',
        type: 'link',
        link: 'http://smarturl.it/CCCLV1',
        infoId: null
    },
    
    {
        chemName: 'Li',
        chemNum: 've',
        title: 'Events',
        time: '',
        color: 'lightGrey',
        type: 'modal',
        infoId: 'liveEvents'
    },

    {
        title: "PERIODIC TABLE",
        size: 'large'
    },

    {
        title: 'Includes vocal elements',
        color: 'blue'
    },

    {
        title: 'Only instrumental elements',
        color: 'mustard'
    },
    

    {
        title: 'Includes visual elements',
        color: 'green'
    },

]