import React from 'react'
import { closeModal } from '../../actions/'
import YouToob from '../YouToob'
import { tileInfo } from '../../data/tileInfo'

class Modal extends React.Component {
    constructor(props) {
        super(props);
        this.escapeKey = this.escapeKey.bind(this);
    }

    componentDidMount(){
        document.addEventListener('keydown', this.escapeKey, false);
    }

    componentWillUnmount(){
        document.removeEventListener('keydown', this.escapeKey, false);
    }

    closeSelf() {
        const {dispatch} = this.props;
        return dispatch(closeModal())
    }

    escapeKey(event) {
        if (event.keyCode === 27) return this.closeSelf();
        return;
    }

    renderYouTubeIframe() {
        const { infoId } = this.props;
        if (!tileInfo[infoId].ytId) return null;
        return (
            <div className="c-modal__video-wrapper">
                <YouToob videoId={tileInfo[infoId].ytId} />
            </div>
        )
    }

    renderText(){
        const {infoId} = this.props;
        if(!tileInfo[infoId].text) return null;
        return tileInfo[infoId].text;
    }
    
    render() {
        const {infoId} = this.props;

        return (
            <div className="c-modal">
                <div className="c-modal__background" onClick={() => this.closeSelf()} />
                <div className="c-modal__content">
                    <div className="c-modal__container">
                        {this.renderYouTubeIframe()}
                        {this.renderText()}
                    </div>
                </div>
            </div>
        )
    }
}


export default Modal
