import React from 'react'
import {config} from '../../constants/config'
import ReactAudioPlayer from 'react-audio-player';
import { stopAudio } from '../../actions/'


const Audio = (props) => (
    <ReactAudioPlayer
        src={`${config.s3}${props.id}.mp3`}
        autoPlay
        onEnded={() => props.dispatch(stopAudio())}
    />
)

export default Audio
