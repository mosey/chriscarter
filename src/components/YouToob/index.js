import React from 'react'

const YouToob = (props) => (
    <iframe 
        src={`https://www.youtube.com/embed/${props.videoId}`}
        frameBorder="0"
        allowFullScreen
    />
)

export default YouToob
