import React from 'react'
import classnames from 'classnames'
import Modal from '../Modal'
import { openModal, playAudio, stopAudio } from '../../actions/'
import { navigateTo } from "gatsby-link"
import audioIcon from '../../images/now-playing-icon.gif'

const Tile = ({...props}) => {

    const renderModal = () => {
        const { dispatch, infoId } = props;
        dispatch(stopAudio());
        
        const modalContent = <Modal
                                dispatch={dispatch}
                                infoId={infoId}
                            />;
        
        return dispatch(openModal(modalContent));
    }

    const { dispatch, audioId, nowPlaying, chemName, chemNum, title, time, type, launchModal } = props;

    const cx = classnames('c-tile', {
        'c-tile--large': props.size === 'large',
        'c-tile--clickable': props.type,
        'c-tile--navy': props.color === 'navy',
        'c-tile--darkNavy': props.color === 'darkNavy',
        'c-tile--blue': props.color === 'blue',
        'c-tile--mustard': props.color === 'mustard',
        'c-tile--lightGrey': props.color === 'lightGrey',
        'c-tile--pink': props.color === 'pink',
        'c-tile--green': props.color === 'green',
        'c-tile--left': props.text === 'left'
    });

    const imageCx = classnames('c-tile__now-playing', {
        'c-tile__now-playing--active': type === 'audio' && audioId === nowPlaying && nowPlaying !== null
    })

    const handleClick = () => {
        if(type==='audio') return handleAudio(audioId);
        if (!props.infoId) return null;
        return renderModal();
    }

    const handleAudio = (id) => {
        if(audioId === nowPlaying) return dispatch(stopAudio());
        return dispatch(playAudio(id))
    }
    
    const tile = <div className={cx} onClick={() => handleClick()}>
        <div className='c-tile__container'>
            
            <img className={imageCx} src={audioIcon} />
            {
                (chemName || chemNum) &&
                <div className="c-tile__chem-info">
                    <span className="c-tile__name">{chemName}</span>
                    <span className="c-tile__num">{chemNum}</span>
                </div>
            }

            {
                title &&
                <span className="c-tile__title">{title}</span>
            }

            {
                time &&
                <span className="c-tile__time">{time}</span>
            }

        </div>
    </div>
    
    if(props.link) return <a href={props.link} className="c-tile__link" target='_blank'>{tile}</a>
    return tile
}
 
export default Tile
