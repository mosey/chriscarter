export const openModal = (payload={}) => {
    return {
        type: 'OPEN_MODAL',
        payload
    }
}

export const closeModal = () => {
    return {
        type: 'CLOSE_MODAL'
    }
}

export const playAudio = (id) => {
    return {
        type: 'PLAY_AUDIO',
        payload: id
    }
}

export const stopAudio = () => {
    return {
        type: 'STOP_AUDIO'
    }
}