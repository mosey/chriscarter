import React from 'react'
import { website, twitter, facebook, instagram } from '../data/icons'

const NotFoundPage = () => (
  <div>

    <div className="c-social">
      <ul className="c-social__list">
        <li><a href="http://www.chriscarter.co.uk " target="_blank">{website}</a></li>
        <li><a href="https://twitter.com/chris_carter_" target="_blank">{twitter}</a></li>
        <li><a href="https://www.facebook.com/chriscarterCCCLV1/" target="_blank">{facebook}</a></li>
        <li><a href="https://www.instagram.com/muterecords/" target="_blank">{instagram}</a></li>
      </ul>
    </div>

    <h1>CHRIS CARTER's CHEMISTRY LESSONS VOLUME 1</h1>

    <h2>Page Not found</h2>

  </div>
)

export default NotFoundPage
