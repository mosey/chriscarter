import React, {Component} from 'react'
import Link from 'gatsby-link'
import Tile from '../components/Tile'
import Audio from '../components/Audio'
import {tileData} from '../data/titleData'
import { Website, Twitter, Facebook, Instagram } from '../data/icons'
import { withPrefix } from 'gatsby-link'
import {connect} from 'react-redux'


@connect((store) => {
  return store
})

class IndexPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalContent: null,
      activeAudio: ''
    }
  }

  resetState() {
    return this.setState({
      modalContent: null,
      activeAudio: ''
    })
  }

  renderTiles() {
    return (
      <div className="l-home-wrapper l-main-wrapper__box-2">

        <div className="c-tile__holder">

          {
            tileData.map((el, i) => {
              if (el.blank) return <Tile key={`tile-${i}`} />

              return (
                <Tile
                  key={`tile-${i}`}
                  title={el.title}
                  color={el.color}
                  chemName={el.chemName || null}
                  chemNum={el.chemNum || null}
                  title={el.title || null}
                  time={el.time || null}
                  type={el.type || null}
                  text={el.text || null}
                  infoId={el.infoId || null}
                  dispatch={this.props.dispatch}
                  size={el.size || null}
                  link={el.link || null}
                  audioId={el.audioId || null}
                  nowPlaying={this.props.audio.id || null}
                />
              )
            })
          }

        </div>

      </div>
    )
  }

  render() {
    const { modal, audio, dispatch } = this.props;

    return (
      <div>

          {
            modal.open &&
            modal.content
          }

          {
            audio.playing &&
              <Audio id={audio.id} dispatch={dispatch} />
          }

        <div className="c-social">
          <ul className="c-social__list">
            <li><a href="http://www.chriscarter.co.uk " target="_blank">{Website}</a></li>
            <li><a href="https://twitter.com/chris_carter_" target="_blank">{Twitter}</a></li>
            <li><a href="https://www.facebook.com/chriscarterCCCLV1/" target="_blank">{Facebook}</a></li>
            <li><a href="https://www.instagram.com/muterecords/" target="_blank">{Instagram}</a></li>
          </ul>
        </div>

          <h1>CHRIS CARTER's CHEMISTRY LESSONS VOLUME 1</h1>
          
          <div className="l-main-wrapper">
            <div className="l-main-wrapper__side l-main-wrapper__box-1">
              <iframe
                src="https://open.spotify.com/embed/artist/4Fw9V88d2NoplsZBOkBWaS"
                width="300"
                height="600"
                frameBorder="0"
              />
            </div>

            {this.renderTiles()}
          <div className="l-main-wrapper__side l-main-wrapper__box-3" />
        </div>
        <div className="l-main-wrapper__footer-text">
          <p>
            <small>
              Chris Carter’s Chemistry Lessons (CCCL) Volume One was created during the period 2010 to 2017 and composed and recorded using a wide ranging set of instruments, disciplines and techniques. The majority of the album was recorded using various modular synthesisers but the working process also included drum machines, digital synthesis and digital signal processing (DSP), voice resynthesis, custom electronics and  field recordings. The album was edited and mastered using a computer based digital audio workstation (DAW). The resulting tracks are a hybridisation of unique tonalities, resonances, timbres, sounds and melodies. STUMM415 Written by Chris Carter, published by Mute Song Limited, Released by Mute. <a href="http://chriscarter.co.uk" target="_blank">chriscarter.co.uk</a> <a href="http://mute.com" target="_blank">mute.com</a>
            </small>
          </p>
        </div>
          
      </div>
    )

  }
}

export default IndexPage
