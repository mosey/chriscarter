module.exports = {
  pathPrefix: `/work/chris`,
  siteMetadata: {
    title: 'Chris Carter Periodic Table',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-postcss-sass`,
      options: {
        precision: 8
      },
    },
  ],
};
